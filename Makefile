all: nnvis/nnvis_ui.py

nnvis/nnvis_ui.py: ui/nnvis.ui Makefile
	pyuic5  -o $@ $< --from-imports
	gsed -i 's/nnimageview/.nnimageview/' $@
	black $@

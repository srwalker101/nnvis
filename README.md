# NN-Vis

Visualise your neural network layers.

[Trello board](https://trello.com/c/ZbK3Jx1v/278-neural-network-visualiser)

![Screenshot](https://gitlab.com/srwalker101/nnvis/raw/dev/.readme/screenshot.png)

## Functionality required

* [x] Ability to run the model through inference to assess the layer activations
* [x] Upload model (model management)
* [x] Upload test image
* [x] Interactive element to select layer
    * select particular filter in convolutional layers
* View:
    * [x] convolutional layers
    * [ ] dense layers
* [ ] Render architecture in a nice way

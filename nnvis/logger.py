import logging


logging.basicConfig(level=logging.WARNING, format="[%(asctime)s] %(name)s:%(message)s")

logger_definitions = [("app", logging.DEBUG)]


def build_logger(name, level):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger


def build_loggers():
    loggers = {name: build_logger(name, level) for (name, level) in logger_definitions}
    return loggers

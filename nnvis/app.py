#!/usr/bin/env python

from . import nnvis_ui as ui
from PyQt5 import QtWidgets, QtGui
import tensorflow.keras as keras
import numpy as np
from .types import ConvLayerDefinition
from .logger import build_loggers
from .qimage_helpers import qimage_to_array, array_to_qpixmap

DEFAULT_MODEL = "example_files/example.h5"
DEFAULT_IMAGE = "example_files/mushroom.jpg"


loggers = build_loggers()


class NNVisApplication(QtWidgets.QMainWindow, ui.Ui_MainWindow):
    def __init__(self, parent=None, debug_mode=False):
        super(NNVisApplication, self).__init__(parent)
        self.setupUi(self)

        self.model = None
        self.prediction_data = None
        self.input_layer_shape = None

        # Observables
        self._layers = []
        self._image = None
        self._chosen_layer_index = None
        self._nfilters = 1
        self._filter = 0

        # Connections

        self.layerList.clicked.connect(self.update_chosen_layer_index)
        self.filterSelector.valueChanged.connect(self.update_chosen_filter)
        self.loadModelButton.clicked.connect(self.choose_model)
        self.loadImageButton.clicked.connect(self.choose_image)

        if debug_mode:
            loggers["app"].debug("DEBUG MODE")
            self._debug_mode()

    # Observables

    @property
    def layers(self):
        return self._layers

    @layers.setter
    def layers(self, value):
        self._layers = value
        self.update_layers_list(self._layers)

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._image = value
        self.update_image(self._image)

    @property
    def chosen_layer_index(self):
        return self._chosen_layer_index

    @chosen_layer_index.setter
    def chosen_layer_index(self, value):
        self._chosen_layer_index = value
        self.display_layer(self._chosen_layer_index)

    @property
    def nfilters(self):
        return self._nfilters

    @nfilters.setter
    def nfilters(self, value):
        self._nfilters = value
        self.filterSelector.setMaximum(self._nfilters)

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, value):
        self._filter = value
        prediction_data = self.prediction_data[:, :, self.filter]
        image = array_to_qpixmap(prediction_data)
        self.update_image(image)

    # Callbacks

    def update_chosen_layer_index(self, model_idx):
        new_row = model_idx.row()
        loggers["app"].info("Chosen layer index changed to %s", new_row)
        self.chosen_layer_index = new_row
        self.filter = 1
        self.filterSelector.setValue(self.filter)

    def update_chosen_filter(self, filter_value):
        loggers["app"].info("Chosen filter changed to %s", filter_value)
        self.filter = filter_value - 1

    def choose_model(self, _checked):
        model_filename = self._select_file("Open model", "Model files (*.h5)")
        if not model_filename:
            return
        self.model = self.load_model(model_filename)

    def choose_image(self, _checked):
        image_filename = self._select_file(
            "Open image", "Image files (*.png *.jpg *.jpeg)"
        )
        if not image_filename:
            return
        self.image = self.load_image(image_filename)

    def _select_file(self, dialog_text, dialog_filter, open_dir=None, options=None):
        open_dir = open_dir if open_dir is not None else ""
        options = options if options is not None else QtWidgets.QFileDialog.Options()

        filename, filter_text = QtWidgets.QFileDialog.getOpenFileName(
            self, dialog_text, open_dir, dialog_filter, options=options
        )
        return filename

    # Helper functions

    def display_layer(self, layer_idx):
        loggers["app"].info("Displaying layer %s", layer_idx)
        layer_definition = self.layers[layer_idx]

        display_model = keras.models.Model(
            inputs=self.model.input,
            outputs=self.model.layers[layer_definition.idx].output,
        )
        prediction = self.predict(display_model, self.image)

        self.show_prediction(prediction)

    def predict(self, model, image):
        loggers["app"].info("Predicting using new model")
        scaled_image = self.reshape_image_for_model(image)
        image_data = qimage_to_array(scaled_image)
        image_data = np.expand_dims(image_data, axis=0)
        return np.squeeze(model.predict(image_data))

    def reshape_image_for_model(self, image):
        if self.input_layer_shape is None:
            raise ValueError("Image shape not known, load model first")

        target_shape = self.input_layer_shape
        return image.scaled(*target_shape[:2]).toImage()

    def show_prediction(self, prediction_data):
        loggers["app"].info("Showing prediction")
        self.prediction_data = prediction_data
        self.nfilters = prediction_data.shape[-1]
        self.filter = 1

    def load_model(self, filename):
        loggers["app"].debug("Loading model %s", filename)
        model_data = keras.models.load_model(filename)
        self.input_layer_shape = model_data.layers[0].input_shape[1:]
        assert len(self.input_layer_shape) == 3
        assert self.input_layer_shape[-1] in {1, 3}
        self.layers = self.parse_layers_from(model_data)
        self.update_model_label(filename)
        return model_data

    def load_image(self, filename):
        im = QtGui.QPixmap(filename)
        self.update_image_label(filename)
        return im

    def update_layers_list(self, layers):
        loggers["app"].info("Updating layers list")
        for layer in layers:
            self.layerList.addItem(layer.list_entry())

    def update_image(self, image):
        loggers["app"].info("Updating image")
        self.outputImageView.set_image(image)

    def update_image_label(self, filename):
        self.loadImageLabel.setText(filename)

    def update_model_label(self, filename):
        self.loadModelLabel.setText(filename)

    def parse_layers_from(self, model_data):
        newlayers = []
        for i, layer in enumerate(model_data.layers):
            if i == 0:
                continue

            if isinstance(layer, keras.layers.Conv2D):
                newlayers.append(ConvLayerDefinition.from_layer(i, layer))
        return newlayers

    # debug functions

    def _debug_mode(self):
        self.model = self.load_model(DEFAULT_MODEL)
        self.image = self.load_image(DEFAULT_IMAGE)

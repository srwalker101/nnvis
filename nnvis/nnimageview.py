from PyQt5 import QtCore, QtGui, QtWidgets


class NNImageView(QtWidgets.QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.scene = QtWidgets.QGraphicsScene()
        self.pixmap_item = QtWidgets.QGraphicsPixmapItem()
        self.scene.addItem(self.pixmap_item)
        self.setScene(self.scene)

        self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)

    def set_image(self, image):
        self.pixmap_item.setPixmap(image)

    def wheelEvent(self, event):
        point = event.pixelDelta()
        if point.y() > 0:
            self.scale(1.1, 1.1)
        else:
            self.scale(0.9, 0.9)

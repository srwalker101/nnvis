class EvaluationError(RuntimeError):
    pass


class NoModelSupplied(EvaluationError):
    def __init__(self):
        super().__init__("No model supplied")


class NoImageSupplied(EvaluationError):
    def __init__(self):
        super().__init__("No image supplied")


class ModelEvaluator(object):
    def __init__(self):
        self.model = None
        self.image = None

    @property
    def valid(self):
        return self.model is not None and self.image is not None

    def predict(self):
        if not self.valid:
            if self.model is None:
                raise NoModelSupplied()
            elif self.image is None:
                raise NoImageSupplied()

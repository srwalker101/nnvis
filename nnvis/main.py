import argparse
import sys
from .app import NNVisApplication
from PyQt5 import QtWidgets


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="Start the application in debug mode (using the example data provided in the repository)",
    )
    args, unparsed_args = parser.parse_known_args()

    app = QtWidgets.QApplication(sys.argv[:1] + unparsed_args)
    form = NNVisApplication(debug_mode=args.debug)
    form.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

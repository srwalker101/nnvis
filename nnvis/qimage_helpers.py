from PyQt5 import QtGui
import numpy as np

__all__ = ["qimage_to_array", "array_to_qpixmap"]


def qimage_to_array(im):
    fmt = im.format()
    bits = im.bits()
    if fmt == QtGui.QImage.Format_RGB32:
        bits.setsize(im.byteCount())
        out_array = np.asarray(bits)
        channels_count = 4
        arr = out_array.reshape((im.height(), im.width(), channels_count))
        arr = arr[:, :, :3]
        return arr.copy()

    else:
        raise NotImplementedError("Images not RGB32 not implemented")


def array_to_qpixmap(arr):
    arr = normalise_and_convert_to_integers(arr)
    height, width = arr.shape
    im = QtGui.QImage(arr.repeat(4), width, height, QtGui.QImage.Format_RGB32)
    return QtGui.QPixmap(im)


def normalise_and_convert_to_integers(arr):
    minval = arr.min()
    maxval = arr.max()

    norm = (arr - minval) / (maxval - minval)
    return (norm * 255).astype(np.uint8)

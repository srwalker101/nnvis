from typing import NamedTuple


class ConvLayerDefinition(NamedTuple):
    name: str
    filters: int
    idx: int

    @classmethod
    def from_layer(cls, idx, layer):
        return cls(name=layer.name, filters=layer.filters, idx=idx)

    def list_entry(self):
        return f"[{self.idx}] {self.name} ({self.filters} filters)"

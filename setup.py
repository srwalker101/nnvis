from setuptools import setup, find_packages

setup(
    name="nnvis",
    version="0.1.0",
    author="Simon Walker",
    author_email="s.r.walker101@googlemail.com",
    packages=find_packages(),
    entry_points={"console_scripts": ["nnvis=nnvis.main:main"]},
)
